require ('dotenv').config();
const express = require('express');
const helmet =require ('helmet');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const cors = require('cors');


const { Pagos } = require('./Mi App Persistente/src/database/sync');
const { Usuarios } = require('./Mi App Persistente/src/database/sync');
const { Productos } = require('./Mi App Persistente/src/database/sync');
const { Pedidos } = require('./Mi App Persistente/src/database/sync');
const { Pedidos_detalle } = require('./Mi App Persistente/src/database/sync');
const { Direcciones } = require('./Mi App Persistente/src/database/sync');
const { estadoPedido } = require('./Mi App Persistente/src/database/sync');

//CONFIGURACION DE SERVIDOR EXPRESS

const app = express();


require ('./Mi App Persistente/src/database/db'); // llamado a la base de datos
require ('./Mi App Persistente/src/database/sync'); // sincronizo

app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors())

app.use('/pagos', require('./Mi App Persistente/src/routes/api/pagos/index.js')) //agregar similar cada vez que quiera crear una ruta para postman
app.use('/usuarios', require('./Mi App Persistente/src/routes/api/usuarios/index.js'))
app.use('/estados', require('./Mi App Persistente/src/routes/api/estados/index.js'))
app.use('/direcciones', require('./Mi App Persistente/src/routes/api/direcciones/index.js')) 
app.use('/suspendido', require('./Mi App Persistente/src/routes/api/suspenderusuario/index.js'))
app.use('/productos', require('./Mi App Persistente/src/routes/api/productos/index.js'))
app.use('/pedidos', require('./Mi App Persistente/src/routes/api/pedidos/index.js'))
app.use('/pedidos_detalle', require('./Mi App Persistente/src/routes/api/pedidos_detalle/index.js'))
app.use('/login', require('./Mi App Persistente/src/routes/api/login/index.js'))

app.listen(3000,()=>{
    console.log("Servidor ejecutandose por el puerto 3000");

});

// CONFIGURACION DE SWAGGER 
//configurar el option
const swaggerOptions =  {
  swaggerDefinition:{
      info: {
          title: 'Delilah Restó',
          version:'2.0.0'
          
      }
  },
  apis:['./swagger.js'],
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use('/delilahdocs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));


