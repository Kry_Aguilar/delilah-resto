//ARCHIVO PRINCIPAL Y UNICO DEL SPRINT 1

const express = require('express');
const server = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
//configuracion de middleware para aceptar json en su formato 
server.use(express.json()) // for parsing application/json
server.use(express.urlencoded({ extended: true })) // for parsing application/x-   www-form-urlencoded
//configurar el option
const swaggerOptions =  {
    swaggerDefinition:{
        info: {
            title: 'Delilah Restó',
            version:'1.0.0'
            
        }
    },
    apis:['./swagger.js'],
};
//configurar el swaggerDocs
const swaggerDocs = swaggerJsDoc(swaggerOptions);
//configuro el server para el uso del swagger, o configurar el endpoint
server.use('/delilahdocs', 
            swaggerUI.serve,
            swaggerUI.setup(swaggerDocs));

///MIDDLEWARES///

// Middleware para validar estados permitidos en modificacion de pedidos 
function valestados (req,res,next){
  const {estadopedido}= req.body;
  if ( estadopedido =="pendiente"|| estadopedido=="confirmado" || estadopedido=="en preparacion" || estadopedido=="enviado" || estadopedido=="entregado" ) {
    next();
  }   else {
      res.send('Ha ingresado un estado inválido');
      };
 }; 

// Middleware para validar campos vacios en Alta de medios de pago -  OK
function valpagos (req,res,next){
  const {descrip_pago}= req.body;
 
  if (!descrip_pago) {
       res.send('debe ingresar todos los datos solicitados')
     }
   else {
      res.send('Alta de producto correcta');
     next();
        };
 }; 

// Middleware para validar campos vacios en Alta de productos -  OK
function validarproduct (req,res,next){
  const {nombre, precio}= req.body;
 
  if (!nombre || !precio) {
       res.send('debe ingresar todos los datos solicitados')
     }
   else {
      res.send('Alta de producto correcta');
     next();
        };
 }; 

//Middleware para validar email duplicados en la creación de nuevos usuarios -  OK

function validaremail (req,res,next){ 
 
  let buscaremail = usuarios.find(emailusu=>emailusu.email==req.body.email);
   if (!buscaremail) {
     next();  
   }
   else {
     res.send('el email ya esta registrado');
   }
}; 

// Middleware para validar campos vacios en nuevo usuario -  OK
function validardatos (req,res,next){
 const {nombre, email, telefono, pass, direccion}=req.body;

 if (!nombre || !email || !telefono || !pass || !direccion) {
      console.log ('debe ingresar todos los datos solicitados');
      res.send('debe ingresar todos los datos solicitados');
    }
  else {
    
    //res.send('Usuario creado con exito');
    next();
       };
}; 

// Midlleware para validar si hay un usuario logueado 
function EstaLog (req,res,next){
  
   const usualog = (usuarios.find(usuabuscado=>usuabuscado.logueado== true));
  
  
    if (!usualog){
      console.log(usualog);
      console.log('el usuario debe loguearse');

    }
    else{
     // res.send ('el usuario ya se encuentra logueado'); 
      next();
    };

};

//Middleware para validar si el usuario logueado es administrador 
function Adminis (req,res,next){ 
  const usualog = (usuarios.find(usuabuscado=>usuabuscado.logueado==true && usuabuscado.admin== true));
 
  console.log('datos de adminis ', usualog);
  
  if (!usualog){
    console.log(usualog);
    res.send('no tiene autorización');
    console.log('no tiene autorización'); 

  }
  else{
   
    next();
  };
};

//ARRAYS CREADAS A MODO DE PRUEBA
   var usuarios = [
    {
       idusuario:1,
       nombre:"Karina Aguilar",
       email:"karina@cliente.com.ar",
       telefono: 12456453,
       direccion:"calle 1 1234",
       admin:true,
       pass: 123,
       logueado: false,

    },
    {
      idusuario:2,
      nombre:"Carlos Perez",
      email:"carlos",
      telefono: 4568552,
      direccion:"calle 30 4565",
      admin:false,
      pass: "123",
      logueado: false,
        
    },
    {
      idusuario:3,
      nombre:"Jose Garcia",
      email:"Jose@cliente.com.ar",
      telefono: 356475,
      direccion:"calle 18 120",
      admin:false,
      pass: "123456",
      logueado: false,
     
    }
];
var productos = [
  {
     idproducto: 1,
     nombre:"Hamburguesa completa",
     precio:"450",

  },
  {
    idproducto: 2,
     nombre:"Lomito completo",
     precio:"550",
      
  },
  {
     idproducto: 3,
     nombre:"Pizza de muzzarella",
     precio:"400",
  }
];

var pedidos = [
  {
     idpedido: 1,
     idusuario: 1,
     detalle: [                                    
      {   
        idproducto: 2,
        cantidad: 2,
       }
  ],
     direccionentrega: "rosales 800",
     estadopedido:"confirmado",
     
  },
  {
    idpedido: 2,
     idusuario: 2,
     detalle: [
       {
        idproducto: 2,
        cantidad: 2,
       }
      ],
        direccionentrega: "san martin 500",
        estadopedido:"pendiente",
     
      
  },
  {
    idpedido: 3,
    idusuario: 2,
    detalle: [
      {
       idproducto: 2,
       cantidad: 3,
      }
     ],
    direccionentrega: "perito moreno 300",
    estadopedido:"pendiente",
    mediopago: "tarjeta de credito",
   
  },
  {
    idpedido: 4,
    idusuario: 1,
    detalle: [
      {
       idproducto: 2,
       cantidad: 3,
      }
     ],
    direccionentrega: "belgrano 500",
    estadopedido:"pendiente",
    mediopago: "efectivo",
    
   
  }
];
var pagos = [
  {
     idpago: 1,
     descrip_pago: "tarjeta de credito",
    },
  {
    idpago: 2,
    descrip_pago: "efectivo",
    
  },
  {
    idpago: 3,
    descrip_pago: "tarjeta de debito",
    mediopago: "cheque",
   
  }];


 server.get('/usuarioslista', (req, res) => { 
  res.status(201).send(usuarios);
});



//b. Los usuarios pueden hacer login con email y contraseña. OK
 server.post('/login', (req, res) => {
  
  // recorrer el array de usuarios para que no hayan dos usuarios logueados
  usuarios.map(item=>{
    item.logueado = false;
  });

  var login_user = {
    email: req.body.email,
    pass: req.body.pass,
  };
  var usuarioencontrado = usuarios.findIndex(usuariobuscado=>usuariobuscado.email==login_user.email && usuariobuscado.pass==login_user.pass);
   if (usuarioencontrado ==-1) {
     res.send('no existe el usuario');
        
   }
   else {
    //cambio el estado del usuario validado al estado logueado.    
    usuarios[usuarioencontrado].logueado = true; 
    res.send('usuario logueado con exito');
    console.log (usuarios[usuarioencontrado]); // muestro por consola los datos del usuario 

   }
});

// a.Los usuarios pueden crear una cuenta en la Aplicación OK 
server.post('/usuarionuevo', validaremail, (req, res) => {
  
  usuarios_new = {
    
    idusuario: usuarios.length +1,
    nombre: req.body.nombre,
    email: req.body.email,
    telefono: req.body.telefono,
    direccion: req.body.direccion,
    pass: req.body.pass,
    admin: false,
    logueado: false,

     };

 respuesta= {
   error: false,
   codigo: 200,
   mensaje:"usuario creado",
   respuesta: usuarios,
 };

  usuarios.push(usuarios_new);
  res.send(respuesta);
  console.log(usuarios_new);
});

//los usuarios registrados pueden ver la lista de productos 
server.get('/productoslista', EstaLog, (req, res) => {
    res.status(201).send(productos);
  });

//f.Los usuarios administradores pueden dar de alta nuevos productos 
  server.post('/productosalta', EstaLog, Adminis, validarproduct, (req, res) => {
  
    producto_new = {
      idproducto: productos[productos.length-1].idproducto+1,
      nombre: req.body.nombre,
      precio:req.body.precio,
      estado: true,
  
       };
 
    productos.push(producto_new);
    console.log(producto_new);
  });

  //h.Los usuarios administradores pueden eliminar un producto 
  server.delete('/borrarproducto', EstaLog, Adminis, (req, res) => {
  
    var borrarprod = {
      nombre : req.body.nombre,
    };
    let prodencontrado = productos.findIndex(productobuscado=>productobuscado.nombre== borrarprod.nombre);
  
    if (prodencontrado ==-1) {
       res.send('no existe el producto que se quiere eliminar');
          
     }
     else {
    productos.splice(prodencontrado);
    res.send('el producto se eliminó con exito');
     }
    });

//g. Modificar datos del producto //
   server.put('/productosmodif', EstaLog, Adminis, (req, res) => {
  
     var modifprod = {idproducto, nombre, precio} = req.body; 
        
      let prodencontrado =productos.findIndex(prodbus=>prodbus.idproducto==modifprod.idproducto);
         if (prodencontrado ==-1) {
     res.send('no existe el producto que se quiere modificar');
        
   }
   else {
   productos[prodencontrado].nombre=modifprod.nombre;   
   productos[prodencontrado].precio=modifprod.precio; 
   productos.push(prodencontrado);
   console.log (productos[prodencontrado]); //muestro por consola el nuevo valor del campo modificado
   res.send('el producto se modificó con exito');
   }
  });




//e. Los administradores pueden ver todos los pedidos 
 server.get('/pedidoslista', EstaLog, Adminis, (req, res) => {
    res.status(201).send(pedidos);
  });

//C. Los usuarios registrados pueden realizar pedidos de los productos OK 
  server.post('/pedidonuevo', EstaLog, (req, res) => {
  var iduslog =usuarios.find(usuariobuscado=>usuariobuscado.logueado==true);
   
   
    const detalle  = req.body.detalle;
    pedido_new = {
      
        idpedido: pedidos[pedidos.length-1].idpedido+1,
        idusuario: iduslog.idusuario,
        direccionentrega: req.body.direccionentrega,   //m. que los usuarios puedan seleccionar medio de pago
        estadopedido: req.body.estadopedido,
        mediopago: req.body.mediopago,          //r. que los usuarios puedan agregar direccion a su pedido
        detalle: detalle                                   
            
      };
  
   respuesta= {
     error: false,
     codigo: 200,
     mensaje:"pedido ingresado correctamente",
     respuesta: pedidos,
   };
   if (pedido_new.direccionentrega ==undefined) {
     pedidos_new.direccionentrega = iduslog.direccion;

   }
  
    pedidos.push(pedido_new);
    res.send(respuesta);
    console.log(pedido_new);
  });


// e. modificar pedido
  server.put('/pedidomodif', EstaLog, valestados, (req, res) => {
    // busco los datos del usuario logueado
    var iduslog =usuarios.find(usuariobuscado=>usuariobuscado.logueado==true);
    
    var modifpedido = {idpedido, direccionentrega, estadopedido, mediopago, detalle} = req.body; 
  //busco el pedido en la base de datos pedidos      
  let pedencontrado = pedidos.findIndex(pedbus=>pedbus.idpedido==modifpedido.idpedido);
     if (pedencontrado ==-1) {
         res.send('no existe el pedido que se quiere modificar')       
        } else {
                // 
                if (pedidos[pedencontrado].idusuario==iduslog.idusuario && iduslog.admin==false) {
                      
                      if (pedidos[pedencontrado].estadopedido == "pendiente") {  
                         
                         pedidos[pedencontrado].estadopedido=modifpedido.estadopedido; 
                         pedidos[pedencontrado].direccionentrega=modifpedido.direccionentrega;  
                         pedidos[pedencontrado].detalle=modifpedido.detalle;
                         pedidos[pedencontrado].mediopago=modifpedido.mediopago;
                         pedidos.push(pedencontrado);
                         console.log (pedidos[pedencontrado]); //muestro por consola el nuevo valor del campo modificado
                         res.send('el pedido se modificó con exito');
                          } else{
                           res.send ('No puede modificar el pedido');                        
                           };
                      
               } else {
                    if (iduslog.admin == false){  
                       res.send ('no tiene permiso para modificar este pedido');
                            
                   } else{
                       pedidos[pedencontrado].estadopedido=modifpedido.estadopedido; 
                       pedidos[pedencontrado].direccionentrega=modifpedido.direccionentrega;  
                       pedidos[pedencontrado].detalle=modifpedido.detalle;
                       pedidos[pedencontrado].mediopago=modifpedido.mediopago;
                       pedidos.push(pedencontrado);
                       console.log (pedidos[pedencontrado]); //muestro por consola el nuevo valor del campo modificado
                       res.send('el pedido se modificó con exito');
                     };                                        
                  };
                  
  };
  
});
             
              //D. Los usuarios registrados pueden ver el historial de sus pedidos 

  server.get('/pedidoshistorial', EstaLog, (req, res) => {
    
    var iduslog =usuarios.find(usuariobuscado=>usuariobuscado.logueado==true);    
  
    let pedencontrado = pedidos.findIndex(pedbuscado=>pedbuscado.idusuario== iduslog.idusuario);
  
    if (pedencontrado ==-1) {
       res.send('no se registran pedidos');
    }
  
      else{
       console.log(pedidos[pedencontrado]);
      }

  
      
  });

  // n. que los administradores puedan crear nuevo medio de pago OK 
  server.post('/pagosalta', EstaLog, Adminis, valpagos, (req, res) => {
  
    pago_new = {
      
        idpago: pagos[pagos.length-1].idpago+1,
        
        descrip_pago: req.body.descrip_pago,
       
      };
      
   respuesta= {
     error: false,
     codigo: 200,
     mensaje:"medio de pago creado correctamente",
     respuesta: pagos,
   };
  
    pagos.push(pago_new);
    res.send(respuesta);
    console.log(pago_new);
  });

// p. que los administradores puedan eliminar medio de pago //
  server.delete('/borrarpago', EstaLog, Adminis, (req, res) => {
  
    var borrarmedpa = {
      descrip_pago : req.body.descrip_pago,
    };
    let pagoencontrado=pagos.findIndex(pagobuscado=>pagobuscado.descrip_pago == borrarmedpa.descrip_pago);
  
    if (pagoencontrado ==-1) {
       res.send('no existe el medio de pago que se quiere eliminar');
          
     }
     else {
    pagos.splice(pagoencontrado);
    res.send('el medio de pago se eliminó con exito');
     };
    });


    

//g. Modificar medio de pago 
server.put('/modificarpago', EstaLog, Adminis, (req, res) => {
  
  var modifpago = {
    idpago: req.body.idpago,
    descrip_pago: req.body.descrip_pago,
  };
  let pagoencontrado=pagos.findIndex(pagobuscado=>pagobuscado.idpago== modifpago.idpago);
  if (pagoencontrado ==-1) {
     res.send('no existe el medio de pago que se quiere modificar');
        
   }
   else {
   pagos[pagoencontrado].descrip_pago=modifpago.descrip_pago;   
   pagos.push(pagoencontrado);
   console.log (pagos[pagoencontrado]); //muestro por consola el nuevo valor del campo modificado
   res.send('el medio de pago se modificó con exito');
   }
  });
  
  //q. Los administradores pueden ver todos los medios de pago 
 server.get('/pagoslista', EstaLog, Adminis,  (req, res) => {
  res.status(201).send(pagos);
});
    
//Llamado a puerto
server.listen(5000, function(){
    console.log('Estoy escuchando por el puerto 5000');
});
