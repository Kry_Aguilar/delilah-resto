# README Delilah Restó

Instrucciones de Instalación

Crear la BD utilizando un gestor como XAMPP, el nombre de la BD es delilahdb. Una vez creada la base de datos las tablas se cargaran al iniciar el servidor con el comando npm run start. 

Instalar todas las dependencias en terminal integrado del archivo index.js.

instalar nodemon (npm i nodemon)
instalar express (npm i express --save)
instalar swagger (npm i swagger-ui-express swagger-jsdoc --save)
instalar cors (npm install cors)
Instalar dotenv (npm i dotenv)
Instalar sequelize (npm install --save sequelize)
Instalar mysql2 (npm i mysql2)
Instalar jsonwebtoken (npm i jsonwebtoken)
Instalar npm install bcrypt 
Instalar Helmet npm install helmet --save
Instalar mocha npm install mocha

Iniciar servidor con el comando (npm run start)


Test
Ejecutar testing con el comando (npm run test) dentro del terminal integrado.


Login
Colocar el token que se genera en el Headers columna Value y en la columna Key de Headers seleccionar Authorization para su funcionamiento en Postman.


Ejecutar swagger en navegador http://localhost:3000/delilahdocs

Repositorio GitLab:  https://gitlab.com/Kry_Aguilar/delilah-resto.git



Alumna: Karina Aguilar




