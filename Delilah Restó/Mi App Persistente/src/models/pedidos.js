const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize} = require ('../database/db');
const Pedidos_detalle = require('./pedidos_detalle');
const Pagos = require ('./pagos');
const Direcciones = require('./direcciones');
const EstadoPedido = require ('./estadoPedido');
const Usuarios = require('./usuarios');

class Pedidos extends Model{}
Pedidos.init ({

    idpedido:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
       
    idusuario: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    iddireccion: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    idestado: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    idpago: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    fecha: {
        type: DataTypes.DATE,
        allowNull: false
    },

},

{
    sequelize, // es necesario para crear el timestamps
    modelName: 'Pedidos',
    timestamps: false, // impide que se creen dos columnas (createdAt,updateAt)
    
});

module.exports = Pedidos;

Pedidos.hasMany(Pedidos_detalle);
Pedidos.hasOne(Pagos);
Pedidos.hasOne(EstadoPedido);
Pedidos.hasOne(Direcciones);
