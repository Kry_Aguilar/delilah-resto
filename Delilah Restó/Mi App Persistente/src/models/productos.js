const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize} = require ('../database/db');

class Productos extends Model{}
Productos.init ({

    idproducto:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
       
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    },

    precio: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

},

{
    sequelize, // es necesario para crear el timestamps
    modelName: 'Productos',
    timestamps: false, // impide que se creen dos columnas (createdAt,updateAt)
    
});

module.exports = Productos;

