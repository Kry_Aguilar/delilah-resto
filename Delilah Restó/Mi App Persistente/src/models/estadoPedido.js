const { DataTypes, Model} = require('sequelize');
const {sequelize} = require ('../database/db');

class EstadoPedido extends Model{}
EstadoPedido.init ({

    idestado:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
       
    descrip_estado: {
        type: DataTypes.STRING,
        allowNull: false
    }
},
{
    sequelize, // es necesario para crear el timestamps
    modelName: 'estadoPedido',
    timestamps: false, // impide que se creen dos columnas (createdAt,updateAt)
    
});

module.exports = EstadoPedido;