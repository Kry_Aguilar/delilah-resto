const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize} = require ('../database/db');
const Direcciones = require('./direcciones');
const Pedidos = require ('./pedidos');

class Usuarios extends Model{}
Usuarios.init ({

    idusuario:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
       
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    telefono: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    direccion: {
        type: DataTypes.STRING,
        allowNull: false
    },

    admin: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },

    pass: {
        type: DataTypes.STRING,
        allowNull: false
    },

    logueado: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },

    suspendido: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },

},
{
    sequelize, // es necesario para crear el timestamps
    modelName: 'Usuarios',
    timestamps: false, // impide que se creen dos columnas (createdAt,updateAt)
    
});

module.exports = Usuarios;


Usuarios.hasMany(Direcciones);
Usuarios.hasMany(Pedidos);


