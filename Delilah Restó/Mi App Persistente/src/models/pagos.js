
const { DataTypes, Model} = require('sequelize');
const {sequelize} = require ('../database/db');

class Pagos extends Model{}
Pagos.init ({

    idpago:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
       
    descrip_pago: {
        type: DataTypes.STRING,
        allowNull: false
    }
},
{
    sequelize, // es necesario para crear el timestamps
    modelName: 'Pagos',
    timestamps: false, // impide que se creen dos columnas (createdAt,updateAt)
    
});

module.exports = Pagos;

 //tabla creada ok, sincronizada ok