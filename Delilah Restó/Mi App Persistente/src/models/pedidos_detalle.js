const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize} = require ('../database/db');
const Productos = require('./productos');


class Detalle extends Model{}
Detalle.init ({

    iddetalle:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },

    idpedido:{
        type: DataTypes.INTEGER,
        allowNull: false
    },
       
    idproducto: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    cantidad: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

},

{
    sequelize, // es necesario para crear el timestamps
    modelName: 'Detalle de Pedido',
    timestamps: false, // impide que se creen dos columnas (createdAt,updateAt)
    
});

module.exports = Detalle;

Detalle.hasMany(Productos);