const { Sequelize, DataTypes, Model} = require('sequelize');
const { sequelize} = require ('../database/db');

class Direcciones extends Model{}
Direcciones.init ({

    iddirecccion:{
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
       
    direccion: {
        type: DataTypes.STRING,
        allowNull: false
    },

    idusuario: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

},

{
    sequelize, // es necesario para crear el timestamps
    modelName: 'Direcciones',
    timestamps: false, // impide que se creen dos columnas (createdAt,updateAt)
    
});

module.exports = Direcciones;