// archivo que carga las tablas de la base de datos

const {Sequelize} = require('sequelize');
const {sequelize} = require('./db');

const {Pagos} = require('../models/pagos'); //crear similar linea 6 para todas las tablas
const {Usuarios} = require('../models/usuarios');
const {Productos} = require('../models/productos');
const {Pedidos} = require('../models/pedidos');
const {Detalle} = require('../models/pedidos_detalle');



sequelize.sync({force:true})  //force true (droop) crea la tabla , si esta en falso, verifica si esta creada y la trae como esta
  .then(()=>{
      console.log('tablas sincronizadas');
    });

module.exports ={
    Pagos,
    Usuarios,
    Productos,
    Pedidos,
    Detalle,
    
}