//require('dotenv').config();
// conexion con la base de datos, utiliza sequelize para conectarse con el archivo index de la carpeta config

const {Sequelize}= require ('sequelize');

const {database} = require ("../../config")

const sequelize = new Sequelize (
    'delilahdb',
    'root', 
    '',
    
    {
        host: 'localhost',
        dialect: 'mysql',       
        query: {raw: true}, // la consulta solo trae los datos que necesito

    }
);

// agregar funcion para validar conexion
async function validar_conexion (){
    try{
        await sequelize.authenticate();
        console.log('La conexión ha sido establecida correctamente')
     } catch (error){
         console.error('Error al conectarse a la base de datos', error);
     }
}

;

module.exports = { sequelize }