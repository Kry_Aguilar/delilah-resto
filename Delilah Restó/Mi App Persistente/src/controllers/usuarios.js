const Usuarios = require ('../models/usuarios');
const bcrypt = require ('bcrypt');
const rounds = require ('../../config/index.js').database;


// LISTAR TODOS LOS USUARIOS
const listarUsuario = async (req, res) => {

    console.log ("listado de Usuarios");
    const usuarios = (await Usuarios.findAll({}) );
    
    res.json(usuarios);
    console.log (usuarios);   
};

//CREAR UN NUEVO USUARIO
const nuevoUsuario = async (req, res) => {

    const datosUsuario = {
        nombre: req.body.nombre,
        email: req.body.email,
        telefono: req.body.telefono,
        direccion: req.body.direccion,
        admin: true,
        pass: bcrypt.hashSync(req.body.pass, Number.parseInt(rounds)),
        logueado: false,
        suspendido: false
    }

    const nuevousuario= await Usuarios.create(datosUsuario);

    res.json ({
        mensaje: 'Nuevo usuario creado correctamente',
        pagos_data: nuevousuario,
    });
};

//ELIMINAR UN USUARIO

const eliminarUsuario = async (req, res) => {
    const {email} =req.body;
    if (!email) {
        res.json('debe ingresar email del usuario a eliminar!!')
      }
      else{

    const bajausuario = await Usuarios.destroy({
        where:
        {email: req.body.email}

    });

    res.json ({
        mensaje: 'El usuario ha sido eliminado con exito',
        pagos_data: bajausuario,
    });
};
};

//MODIFICAR LOS DATOS DE UN USUARIO

const modificarUsuario = async (req, res) => {

    const {nombre, telefono, direccion, pass, email} =req.body;
    if (!email || !pass || !direccion || !telefono || !nombre) {
        res.json('los campos solicitados son obligatorios!!')
      }
      else{

      const modificausuario = await Usuarios.update(
        {nombre: req.body.nombre,
         telefono: req.body.telefono,
         direccion: req.body.direccion,
         pass: req.body.pass,
        },
        {where:{email: req.body.email}}
        )
    
    res.json ({
        mensaje: 'Datos de Usuario modificados correctamente',
        pagos_data: modificausuario,
    });
};

};



module.exports = {
    listarUsuario,
    nuevoUsuario,
    eliminarUsuario,
    modificarUsuario,
    

};