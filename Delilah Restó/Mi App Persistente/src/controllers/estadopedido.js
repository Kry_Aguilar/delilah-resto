const EstadoPedido = require('../models/estadoPedido');


// LISTAR TODOS LOS MEDIOS DE PAGO
const estadopedidos = async (req, res) => {
    
    console.log ("listado de estados de los pedidos");
    const estados = (await EstadoPedido.findAll({}) );
    

    res.json(estados);
    console.log (estados);
    
};


//CREAR un nuevo estado

const crearEstado = async (req, res) => {
    const {descrip_estado} =req.body;
    if (!descrip_estado) {
        res.json('debe ingresar una descripcion de Estado !!')
      }
      else{

    const estadodescripcion ={
        descrip_estado: req.body.descrip_estado
    }
    const nuevoEstado = await EstadoPedido.create(estadodescripcion);

    res.json ({
        mensaje: 'Alta de estado de pedido realizado correctamente',
        pagos_data: nuevoEstado,
    });
};
};

module.exports = {
    crearEstado,
    estadopedidos,

    
};