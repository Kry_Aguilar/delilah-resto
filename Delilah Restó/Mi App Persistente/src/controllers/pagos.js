const Pagos = require ('../models/pagos');

// LISTAR TODOS LOS MEDIOS DE PAGO
const listar = async (req, res) => {
    
    console.log ("listado de formas de pago");
    const pagos = (await Pagos.findAll({}) );
    

    res.json(pagos);
    console.log (pagos);
    
};


//CREAR UNA NUEVA FORMA DE PAGO

const crearPago = async (req, res) => {
    const {descrip_pago} =req.body;
    if (!descrip_pago) {
        res.json('debe ingresar forma de pago!!')
      }
      else{

    const datosPago ={
        descrip_pago: req.body.descrip_pago
    }
    const nuevopago = await Pagos.create(datosPago);

    res.json ({
        mensaje: 'Alta de forma de pago realizada correctamente',
        pagos_data: nuevopago,
    });
};
};

//ELIMINAR UNA FORMA DE PAGO 

const eliminarPago = async (req, res) => {

    const {descrip_pago} =req.body;
    if (!descrip_pago) {
        res.json('debe ingresar forma de pago a eliminar!!')
      }
      else{

    const bajapago = await Pagos.destroy({
        where:
        {descrip_pago: req.body.descrip_pago}

    });

    res.json ({
        mensaje: 'Baja de forma de pago realizada correctamente',
        pagos_data: bajapago,
    });
};
};

//MODIFICAR UNA FORMA DE PAGO

const modificarPago = async (req, res) => {
    const {descrip_pago, idpago} =req.body;
    if (!descrip_pago || !idpago) {
        res.json('debe ingresar forma de pago!!')
      }
      else{

      const modificapago = await Pagos.update(
        {descrip_pago: req.body.descrip_pago},
        {where:{idpago: req.body.idpago}}
        )
    
    res.json ({
        mensaje: 'Modificación de forma de pago realizada correctamente',
        pagos_data: modificapago,
    });
};
};

module.exports = {
    listar,
    crearPago,
    eliminarPago,
    modificarPago,
};