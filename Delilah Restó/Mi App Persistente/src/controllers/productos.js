const Productos = require ('../models/productos');

// LISTADO DE PRODUCTOS
const listadoProductos = async (req, res) => {
    
    console.log ("listado de Productos");
    const productos = (await Productos.findAll({}) );
    
    res.json(productos);
    console.log (productos);
};

//DAR DE ALTA UN NUEVO PRODUCTO

const nuevoProducto = async (req, res) => {
   const  {nombre, precio}= req.body;

    if (!nombre ||!precio) {
        res.json('debe ingresar todos los campos solicitados!!')
      }
      else{

    
    const datosProducto ={
        nombre: req.body.nombre,
        precio: req.body.precio
    }

    const nuevoproducto = await Productos.create(datosProducto);

    res.json ({
        mensaje: 'Nuevo producto cargado correctamente',
        pagos_data: nuevoproducto,
    });
};
};

//ELIMINAR UN PRODUCTO

const eliminarProducto = async (req, res) => {

    const  {nombre}= req.body;

    if (!nombre ) {
        res.json('debe ingresar nombre de productos a eliminar!!')
      }
      else{
    const bajaproducto = await Productos.destroy({
        where:
        {nombre: req.body.nombre}

    });

    res.json ({
        mensaje: 'Se ha eliminado el producto correctamente',
        pagos_data: bajaproducto,
    });
};
 };

//MODIFICAR UN PRODUCTO

const modificarProducto = async (req, res) => {

    const  {nombre, precio}= req.body;

    if (!nombre ||!precio) {
        res.json('los campos solicitados son obligatorios!!')
      }
      else{

      const modificaproducto = await Productos.update(
        {nombre: req.body.nombre,
        precio: req.body.precio},
        {where:{idproducto: req.body.idproducto}}
        )
    
    res.json ({
        mensaje: 'Modificación de Producto exitosa',
        pagos_data: modificaproducto,
    });
};
};

module.exports = {
    listadoProductos,
    nuevoProducto,
    eliminarProducto,
    modificarProducto,
}


