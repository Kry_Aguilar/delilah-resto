const Usuarios = require ('../models/usuarios');

const suspendido = async (req, res) => {

    const suspender = await Usuarios.update(
      {suspendido: true
      },
      {where:{email: req.body.email}}
      )
  
  res.json ({
      mensaje: 'Usuario suspendido correctamente',
      pagos_data: suspender,
  });

};

module.exports = {
    suspendido
};