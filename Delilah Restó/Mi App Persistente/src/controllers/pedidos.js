
const Pedidos = require ('../models/pedidos');
const Detalle = require('../models/pedidos_detalle');

// LISTAR LOS PEDIDOS DE CLIENTES 
const listarPedidos = async (req, res) => {

    console.log ("listado de Pedidos de Clientes");
    const pedidos = (await Pedidos.findAll({}) );
    
    res.json(pedidos);
    console.log (pedidos);
    
};

//CREAR UN NUEVO PEDIDO

const nuevoPedido = async (req, res) => {

    const datosPedidoNuevo = {
        idusuario: req.body.idusuario,
        iddireccion: req.body.iddireccion,
        idestado: req.body.idestado,
        idpago: req.body.idpago,
        fecha: req.body.fecha,
        
      };
      
      
      const datosdetalleNuevo={
        idpedido: req.body.idpedido,
        idproducto: req.body.idproducto,
        cantidad: req.body.cantidad,
      };

    const PedidoNuevo= await Pedidos.create(datosPedidoNuevo);
    const DetalleNuevo= await Detalle.create(datosdetalleNuevo);

    res.json ({
        mensaje: 'Nuevo Pedido creado correctamente',
        pagos_data: [PedidoNuevo, DetalleNuevo]
    });
};

//ELIMINAR UN PEDIDO

const eliminarPedido = async (req, res) => {

    const bajapedido = await Pedidos.destroy({
        where:
        {idpedido: req.body.idpedido}

    });

    res.json ({
        mensaje: 'El Pedido seleccionado ha sido eliminado con exito',
        pagos_data: bajapedido,
    });
};

//MODIFICAR DATOS DE UN PEDIDO

const modificarPedido = async (req, res) => {

      const modificapedido = await Pedidos.update(
        {idpedido: req.body.idpedido,
         idusuario: req.body.idusuario,
         iddireccion: req.body.iddireccion,
         idestado: req.body.idestado,
         idpago: req.body.idpago,
        },
        {where:{idpedido: req.body.idpedido}}
        )
    
    res.json ({
        mensaje: 'Datos de Pedido modificados correctamente',
        pagos_data: modificapedido,
    });
};

module.exports = {
    listarPedidos,
    nuevoPedido,
    eliminarPedido,
    modificarPedido,
}