const Direcciones = require ('../models/direcciones');

// listar Direcciones
const agenda = async (req, res) => {
    
    const direccion = (await Direcciones.findAll({where:{idusuario: req.body.idusuario}}) );
    
    console.log("Agenda de Direcciones");
    res.json(direccion);
    console.log (direccion);
    
};


//Agregar Nueva Direccion a la Agenda de Usuario

const agregardireccion = async (req, res) => {
    const {direccion, idusuario} =req.body;
    if (!direccion || !idusuario) {
        res.json('debe ingresar todos los datos solicitados!!')
      }
      else{

    const datosdireccion ={
        idusuario: req.body.idusuario,
        direccion: req.body.direccion
    }
    const nuevadireccion = await Direcciones.create(datosdireccion);

    res.json ({
        mensaje: 'Nueva direccion agregada a la Agenda',
        pagos_data: nuevadireccion,
    });
};
};



module.exports = {
    agenda,
    agregardireccion,
};