
const Detalle = require ('../models/pedidos_detalle');


// LISTAR detalle de Pedido
const listardetalle = async (req, res) => {
      
    const detallepedido = (await Detalle.findAll({where:{idpedido: req.body.idpedido}}) );
    
    console.log ("detalle de pedido seleccionado");
    res.json(detallepedido);
    console.log (detallepedido);
    
};

const agregarcarrito = async (req, res)=>{
    const {idpedido, idproducto, cantidad}= req.body
    if (!idpedido || !idproducto || !cantidad){
        res.json('debe ingresar todos los datos solicitados!!!')
    }
    else{
        const datoscarrito = {
            idpedido: req.body.idpedido,
            idproducto: req.body.idproducto,
            cantidad: req.body.cantidad
        }
        const carrito = await Detalle.create(datoscarrito);

        res.json({
            mensaje: 'Nuevo producto agregado al pedido satisfactoriamente',
            pagos_data: carrito,
        });
    };
};


const modificarcarrito = async (req, res)=>{
    const {idpedido, idproducto, cantidad}= req.body
    if (!idpedido || !idproducto || !cantidad){
        res.json('debe ingresar todos los datos solicitados!!!')
    }
    else{
        const carritomod = await Detalle.update(
           {idpedido: req.body.idpedido,
            idproducto: req.body.idproducto,
            cantidad: req.body.cantidad,
        }, {where:{iddetalle: req.body.iddetalle}}
        )

        res.json({
            mensaje: 'Modificacion de producto realizado con exito!',
            pagos_data: carritomod,
        });
    };
};

const eliminarcarrito = async (req, res)=>{
    const {iddetalle}= req.body
    if (!iddetalle){
        res.json('debe ingresar todos los datos solicitados!!!')
    }
    else{
        const carritoeliminar = await Detalle.destroy(
        {where:{iddetalle: req.body.iddetalle}}
        )

        res.json({
            mensaje: 'Producto del carrito eliminado con exito!',
            pagos_data: carritoeliminar,
        });
    };
};


module.exports ={

    listardetalle,
    agregarcarrito,
    modificarcarrito,
    eliminarcarrito,
};