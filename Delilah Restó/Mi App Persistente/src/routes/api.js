const router = require ('express').Router();

const apiPagos = require ('./api/pagos/'); //importar las rutas de la carpeta pagos
const apiUsuarios = require ('./api/usuarios/');
const apiProductos = require ('./api/productos/');
const apiPedidos = require ('./api/pedidos/');
const apiDetalle = require ('./api/pedidos_detalle');

router.use('/pagos',apiPagos);
router.use('/usuarios',apiUsuarios);
router.use('/productos',apiProductos);
router.use('/pedidos',apiPedidos);
router.use('/detalle',apiDetalle);




module.exports= router;

//para encriptar la contraseña
/*const { createHmac } = await import('crypto');

const secret = 'abcdefg';
const hash = createHmac('sha256', secret)
               .update('I love cupcakes')
               .digest('hex');
console.log(hash);
// Prints:
//   c0fa1bc00531bd78ef38c628449c5102aeabd49b5dc3a2a516ea6ea959d6658e

*/

