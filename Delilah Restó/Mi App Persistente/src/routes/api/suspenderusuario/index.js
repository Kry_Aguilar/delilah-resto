const router = require ('express').Router(); //Creo una constante para enrutar el endpoint


const { 
    suspendido

} = require ('../../../controllers/supenderusuario'); 
const { autenticarUsuario } = require('../../../middleware/login');
const { estaLog, esAdmin } = require('../../../middleware/usuarios');


router.put('/', autenticarUsuario, estaLog, esAdmin,  suspendido);


module.exports = router;
