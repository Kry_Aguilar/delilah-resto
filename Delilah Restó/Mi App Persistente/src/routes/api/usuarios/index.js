const router = require ('express').Router(); //Creo una constante para enrutar el endpoint
//const autenticarUsuario = require ('../../../middleware/login')
const { estaLog, esAdmin, validaremail, estaSuspendido } = require('../../../middleware/usuarios.js');

const {
    listarUsuario, 
    nuevoUsuario,
    eliminarUsuario,
    modificarUsuario,
    
    //login

} = require ('../../../controllers/usuarios'); 
const { autenticarUsuario } = require('../../../middleware/login.js');

// listado de usuarios desde controllers


router.get('/',estaLog, esAdmin, autenticarUsuario, listarUsuario);

router.post('/',validaremail, nuevoUsuario);

router.delete('/', estaLog, esAdmin, autenticarUsuario, eliminarUsuario);

router.put('/', estaLog, estaSuspendido, autenticarUsuario, modificarUsuario);

module.exports = router;
