const router = require ('express').Router(); //Creo una constante para enrutar el endpoint

const { estaLog, esAdmin } = require('../../../middleware/usuarios.js');
const { estadopedidos, crearEstado } = require ('../../../controllers/estadopedido'); 
const { autenticarUsuario } = require('../../../middleware/login.js');



// listado de pagos desde controllers

router.get('/', estaLog, esAdmin, autenticarUsuario, estadopedidos);

router.post('/', estaLog, esAdmin, autenticarUsuario, crearEstado);


module.exports = router;
