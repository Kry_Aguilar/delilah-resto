const router = require ('express').Router(); //Creo una constante para enrutar el endpoint

const { estaLog, estaSuspendido } = require('../../../middleware/usuarios.js');

const { autenticarUsuario } = require('../../../middleware/login.js');
const { agenda, agregardireccion } = require('../../../controllers/direcciones.js');



//  desde controllers

router.get('/', estaLog, autenticarUsuario, estaSuspendido, agenda);

router.post('/', estaLog, autenticarUsuario, estaSuspendido, agregardireccion);


module.exports = router;
