const router = require ('express').Router(); //Creo una constante para enrutar el endpoint

const { estaLog, esAdmin, estaSuspendido } = require('../../../middleware/usuarios.js');
const {
    listadoProductos, 
    nuevoProducto,
    eliminarProducto,
    modificarProducto
} = require ('../../../controllers/productos'); 
const { autenticarUsuario } = require('../../../middleware/login.js');



// listado de pagos desde controllers

router.get('/',autenticarUsuario ,estaLog, estaSuspendido, listadoProductos);

router.post('/', autenticarUsuario, estaLog, esAdmin, nuevoProducto);

router.delete('/',autenticarUsuario, estaLog, esAdmin, eliminarProducto);

router.put('/', autenticarUsuario, estaLog, esAdmin, modificarProducto);

module.exports = router;

