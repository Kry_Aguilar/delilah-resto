const router = require ('express').Router(); 


const {
    agregarcarrito,
    listardetalle,
    modificarcarrito,
    eliminarcarrito,

}= require ('../../../controllers/pedidos_detalle');
const { autenticarUsuario } = require('../../../middleware/login');
const { pedidocerrado } = require('../../../middleware/pedidos');
const { estaLog, estaSuspendido } = require('../../../middleware/usuarios');


router.get('/', autenticarUsuario, estaLog , estaSuspendido, listardetalle);

router.put('/', autenticarUsuario, estaLog, estaSuspendido, pedidocerrado, modificarcarrito);

router.post ('/', autenticarUsuario, estaLog, estaSuspendido, pedidocerrado, agregarcarrito);

router.delete ('/',autenticarUsuario, estaLog, estaSuspendido, pedidocerrado, eliminarcarrito);

module.exports = router;