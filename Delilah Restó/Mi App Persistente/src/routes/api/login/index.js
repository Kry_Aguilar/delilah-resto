const router = require ('express').Router(); //Creo una constante para enrutar el endpoint
const {autenticarUsuario} = require ('../../../middleware/login')

const {
    
    login

} = require ('../../../controllers/login'); 

const { estaSuspendido } = require('../../../middleware/usuarios');


router.post('/', autenticarUsuario, estaSuspendido, login);


module.exports = router;
