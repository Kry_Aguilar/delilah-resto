const router = require ('express').Router(); //Creo una constante para enrutar el endpoint

const { estaLog, esAdmin } = require('../../../middleware/usuarios.js');
const {
    listar, 
    crearPago,
    eliminarPago,
    modificarPago
} = require ('../../../controllers/pagos'); 
const { autenticarUsuario } = require('../../../middleware/login.js');



// listado de pagos desde controllers

router.get('/', estaLog, esAdmin, autenticarUsuario, listar);

router.post('/', estaLog, esAdmin, autenticarUsuario, crearPago);

router.delete('/', estaLog, esAdmin, autenticarUsuario,eliminarPago);

router.put('/', estaLog, esAdmin, autenticarUsuario,modificarPago);

module.exports = router;

