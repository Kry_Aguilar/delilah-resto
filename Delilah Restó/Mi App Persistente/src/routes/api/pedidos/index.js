const router = require ('express').Router(); //Creo una constante para enrutar el endpoint

const { estaLog, esAdmin, estaSuspendido } = require('../../../middleware/usuarios.js');
const {
    listarPedidos, 
    nuevoPedido,
    eliminarPedido,
    modificarPedido
} = require ('../../../controllers/pedidos'); 
const { autenticarUsuario } = require('../../../middleware/login.js');
const { pedidocerrado } = require('../../../middleware/pedidos.js');


// listado de pedidos desde controllers

router.get('/', estaLog, esAdmin, listarPedidos);

router.post('/', autenticarUsuario ,estaLog, estaSuspendido, nuevoPedido);

router.delete('/',autenticarUsuario, estaLog, esAdmin, eliminarPedido);

router.put('/', autenticarUsuario, estaLog, estaSuspendido, pedidocerrado, modificarPedido);

module.exports = router;
