const express = require ('express');
const Usuarios = require ('../models/usuarios.js');
const jwt = require ('jsonwebtoken');
const {key} = require ('../../config/index.js').database

function autenticarUsuario(req, res, next) {
    try {
        const token = req.headers.authorization.replace('Bearer', '');
        jwt.verify(token, key);
        next();
    } catch (error) {
        res.status(406).send('Error al validar usuario');
    }
};


module.exports = {
    autenticarUsuario,
    
  
}