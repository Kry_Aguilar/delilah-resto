const Usuarios = require('../models/usuarios');

const estaLog = async (req, res, next) => {

    const Logueado = await Usuarios.findOne (
       {
           where: {logueado: true}
       })
       if(!Logueado){
           res.send('Debe registrarse para continuar!')
           res.json('Debe registrarse para continuar!')
       }else{
           next();
       };
    };

const esAdmin = async (req, res,next)=>{
    const administrador = await Usuarios.findOne(
        {where: {admin : true}})

        if (!administrador){
            res.send ('Esta operación requiere permiso de Administrador');
            res.json ('Esta operación requiere permiso de Administrador');
        } else{
            next();
        };
};

const validaremail = async (req, res, next) => {

    const buscarusuario = await Usuarios.findOne (
       {
           where: {email: req.body.email}
       })
       if(!buscarusuario){
          next()
       }else{
           res.send('El email ingresado ya se encuentra registrado!!')
           res.json('El email ingresado ya se encuentra registrado!!')
       };
    };

    const estaSuspendido = async (req, res,next)=>{
        const suspendidoencontrado = await Usuarios.findOne(
            {where: {suspendido : true}})
    
            if (!suspendidoencontrado){
               next();
            }else{
                res.send('El usuario se encuentra suspendido, consulte con su Administrador')
               res.json ('El usuario se encuentra suspendido, consulte con su Administrador')
            
            };
    };

module.exports ={
    estaLog,
    esAdmin,
    validaremail,
    estaSuspendido,
}
