
const chai = require('chai');
const expect = require('chai').expect;
const chaihttp = require('chai-http');
const url= 'http://localhost:3000';



describe('Testeo de Registro de Usuarios',function() {
    it('Usuario Nuevo', (done) => {
    chai.request(url)
    .post('/usuarios/nuevoUsuario')
    .send({
        nombre: "testing",
        email: "testing@email",
        telefono: 11111,
        direccion: "testeando 123",
        admin: false,
        pass: "contrasetest",
        logueado: false,
        suspendido: false
    })
    .end(function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });


