/**
 * @swagger
 * /usuarios:
 *  get:
 *    description: listado de usuarios permitido solo para Administrador
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /login:
 *  post:
 *    description: LogIn de Usuario Registrado
 *    parameters:
 *    - name: email
 *      description: Email del usuario Registrado
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Password del usuario Registrado
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger

* /usuarios:
*  post:
 *    description: Registro de nuevo usuario
 *    parameters:
 *    - name: nombre
 *      description: Nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
  *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: telefono
 *      description: Telefono del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: direccion
 *      description: Direccion del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Password del usuario registrado
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger

* /usuarios:
*  delete:
 *    description: Eliminar un usuario, operacion permitida para Administradores
 *    parameters:
  *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /suspenderusuario:
 *  put:
 *    description: Cambiar estado a Suspendido de Un usuario
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /productos:
 *  get:
 *    description: Listado de Productos
 *    responses:
 *      200:
 *        description: Success
 * 
 */
/**
 * @swagger
 * /productos:
 *  post:
 *    description: Alta de Productos
 *    parameters:
 *    - name: nombre
 *      description: Nombre del producto
 *      in: formData
 *      required: true
 *      type: string
  *    - name: precio
 *      description: Precio del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /productos:
 *  delete:
 *    description: Baja de Producto
 *    parameters:
 *    - name: nombre
 *      description: Nombre del producto
 *      in: formData
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /productos:
 *  put:
 *    description: Modificacion de Producto
 *    parameters:
*    - name: idproducto
 *      description: Identificador del producto
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del producto
 *      in: formData
 *      required: true
 *      type: string
  *    - name: precio
 *      description: Precio del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /pedidos:
 *  get:
 *    description: Listado de Pedidos
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /pedidos:
 *  post:
 *    description: Alta de Pedidos
 *    parameters:
 *    - name: fecha
 *      description: Fecha en la que se genera un nuevo pedido
 *      in: formData
 *      required: true
 *      type: Date
 *    - name: idusuario
 *      description: Usuario que realiza el pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: iddetalle
 *      description: Identificador del detalle del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: idpago
 *      description: Identificador de la forma de pago elegida por el cliente
 *      in: formData
 *      required: true
 *      type: integer
  *    - name: idproducto
 *      description: Identificador del producto solicitado por el cliente
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: cantidad
 *      description: cantidad del producto solicitado por el cliente
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: iddireccion
 *      description: direccion de entrega del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: idestado
 *      description: estado del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /pedidos:
 *  put:
 *    description: Modificacion de datos generales de Pedidos
 *    parameters:
 *    - name: idpedido
 *      description: Identificador del pedido que se quiere modificar
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: idusuario
 *      description: Usuario que realiza el pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: idpago
 *      description: Identificador de la forma de pago elegida por el cliente
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: iddireccion
 *      description: direccion de entrega del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: idestado
 *      description: estado del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /pagos:
 *  post:
 *    description: Alta de medio de pago
 *    parameters:
 *    - name: descrip_pago
 *      description: Nombre del Medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /pagos:
 *  delete:
 *    description: Baja de medio de pago
 *    parameters:
 *    - name: descrip_pago
 *      description: Nombre del Medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /pagos:
 *  put:
 *    description: Modificar medio de pago
 *    parameters:
 *    - name: descrip_pago
 *      description: Nombre del Medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /pagos:
 *  get:
 *    description: Listado de medio de pago
 *    parameters:
 *    - name: descrip_pago
 *      description: Nombre del Medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /direcciones:
 *  get:
 *    description: Listado de direcciones de un cliente
*    parameters:
 *    - name: idusuario
 *      description: Identificador del usuario que requiere sus direcciones
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /direcciones:
 *  post:
 *    description: Agregar una nueva direccion a la agenda
*    parameters:
 *    - name: idusuario
 *      description: Identificador del usuario que requiere sus direcciones
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: direccion
 *      description: Direccion a agregar a la agenda
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /estados:
 *  get:
 *    description: Listado de Estados de Pedido
 *    responses:
 *      200:
 *        description: Success
 * 
 */

/**
 * @swagger
 * /estados:
 *  post:
 *    description: Agregar un nuevo estado
*    parameters:
 *    - name: descrip_estado
 *      description: Descripcion del nuevo estado de pedido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */
